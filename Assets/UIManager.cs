﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CFGUI;
using GUIImplementation;

public class UIManager : MonoBehaviour
{
	void Start ()
    {
        GUIBindings.Init();
        
        m_view = gameObject.AddComponent<GUIView>();
        
        m_guiAsset = Resources.Load<GUIAsset>("GUIAssets/TestGUI");
        
        StartCoroutine(CreateGUI());
    }

    IEnumerator CreateGUI()
    {
        yield return new WaitForSeconds(0.5f);

        //var e = GUIBindings.Create_HUB(m_guiAsset, null);
        var gui = GUIBindings.Create_TestGUI(m_guiAsset, null);
        m_view.SetGUI(gui.Root);
    }

    void OnGUI()
    {
        GUILayout.Label(m_view.DrawcallCount.ToString());

        int limit = Mathf.RoundToInt(m_renderLimit * m_view.DrawcallCount);
        limit = Mathf.RoundToInt(GUILayout.HorizontalSlider(limit, 0, m_view.DrawcallCount, GUILayout.Width(200)));
        m_renderLimit = (float)limit / m_view.DrawcallCount;

        m_view.DrawcallLimit = Mathf.RoundToInt(m_renderLimit * m_view.DrawcallCount);
    }

    private float m_renderLimit;

    private GUIAsset m_guiAsset;
    private GUIView m_view;

}
