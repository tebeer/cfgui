﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

using CFGUI;

namespace CFUI
{
    [CustomEditor(typeof(UIAsset))]
    public class UIAssetInspector : Editor
    {
        //private GUIEditor m_guiWindow;

        void OnEnable()
        {
            m_UIAsset = (UIAsset)target;

            m_system = new UISystem();

            //m_root = m_GUIAsset.Deserialize();

            //if (m_root == null)
            //{
            //    m_root = new GUIEntity();
            //    m_root.name = "Root";
            //    m_root.visible = false;
            //    m_root.enabled = true;
            //    m_root.width = 128;
            //    m_root.height = 128;
            //    m_root.alignment.x = HorizontalAlignment.Stretch;
            //    m_root.alignment.y = VerticalAlignment.Stretch;
            //}
        }

        void OnDisable()
        {
        }

        private void UpdateGUIWindow()
        {
            //if (m_guiWindow == null)
            //    return;
            //
            //m_guiWindow.Repaint();
        }

        public override void OnInspectorGUI()
        {
            if (m_system == null)
            {
                EditorGUILayout.LabelField("Failed to deserialize root");
                return;
            }

            //if (m_guiWindow == null)
            //{
            //    var windows = Resources.FindObjectsOfTypeAll<GUIEditor>();
            //    if (windows.Length > 0)
            //    {
            //        m_guiWindow = windows[0];
            //        m_guiWindow.SetInspector(this);
            //    }
            //}
            //
            //if(m_guiWindow == null)
            //{
            //    if (GUILayout.Button("Open GUI Window"))
            //    {
            //        m_guiWindow = EditorWindow.GetWindow<GUIEditor>();
            //        m_guiWindow.SetInspector(this);
            //    }
            //}

            DrawSidePanel();

            if (GUI.changed)
            {
                UpdateGUIWindow();
            }
        }

        void DrawSidePanel()
        {

            EditorGUILayout.BeginVertical();


            EditorGUILayout.LabelField("CF GUI Editor");
            //EditorGUILayout.LabelField("CF GUI Editor " + m_system.width + "x" + m_system.height);

            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_assetReferences"), true);

            if (GUILayout.Button("Save"))
            {
                m_UIAsset.Serialize(m_system);
            }

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("New"))
            {
                var newIndex = m_system.AddChild(m_selected);
                m_system.Nodes[newIndex].name = "New node" + Random.value.ToString();
            }

            if (GUILayout.Button("Clone"))
            {
                //var newIndex = m_system.AddTwin(m_selected);
                //m_system.Nodes[newIndex].name = "sdfnode";

                //if (m_selected != m_system)
                //{
                //    string json = JsonFx.Json.JsonWriter.Serialize(m_selected);
                //
                //    var newElement = JsonFx.Json.JsonReader.Deserialize<GUIEntity>(json);
                //
                //    newElement.parent = m_selected.parent;
                //    m_selected.parent.children.Insert(m_selected.parent.children.IndexOf(m_selected) + 1, newElement);
                //    m_selected = newElement;
                //}
            }

            if (GUILayout.Button("Delete"))
            {
                //if (m_selected != null && m_selected != m_system)
                //{
                //    m_selected.parent.children.Remove(m_selected);
                //    m_selected = null;
                //}
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();

            //selected = GUILayout.SelectionGrid(selected, new string[] { "a", "b", "c" }, 2);
            //GUI.backgroundColor = Color.gray;

            EditorGUILayout.BeginHorizontal();

            m_hierarchyScroll = EditorGUILayout.BeginScrollView(m_hierarchyScroll, EditorStyles.textArea);//, null, null, EditorStyles.helpBox);

            int index = 0;
            HierarchyGUI(ref index, ref m_system.Nodes[index], new Rect(), false);
            EditorGUILayout.EndScrollView();
            //GUI.backgroundColor = Color.white;

            m_inspectorScroll = EditorGUILayout.BeginScrollView(m_inspectorScroll, EditorStyles.textArea);

            //if (m_selected != null)
                ElementEditor(ref m_system.Nodes[m_selected]);
            //else
            //    EditorGUILayout.LabelField("Nothing selected");

            EditorGUILayout.EndScrollView();

            EditorGUILayout.EndHorizontal();
        }

        void HierarchyGUI(ref int index, ref UINode node, Rect parentRect, bool drawIndent)
        {
            EditorGUILayout.BeginHorizontal();

            //if (m_selected == element)
            //    GUI.backgroundColor = Color.blue;

            //InitStyles();

            //var style = m_hierarchy;

            //GUI.backgroundColor = Color.white;
            if (m_selected == index)
            {
                //style = m_hierarchySelected;
                //GUI.backgroundColor = new Color(.25f,.5f,1,.25f);
                //ind -= 12;
            }

            GUILayout.Space(indent);
            //if (GUILayout.Button(element.children.Count.ToString(), style, GUILayout.Width(indent)))
            //{
            //    SetShowChildren(element, !GetShowChildren(element));
            //}
            //element.enabled = EditorGUILayout.Foldout(element.enabled, element.name);
            //EditorGUILayout.Foldout(GetShowChildren(element), "");

            if (GUILayout.Button(node.name + " " + index + " " + node.childCount, EditorStyles.label))
                m_selected = index;

            var rect = GUILayoutUtility.GetLastRect();
            rect.yMin--;
            rect.yMax++;

            if (m_lineMaterial == null)
                m_lineMaterial = new Material(Shader.Find("Sprites/Default"));

            m_lineMaterial.SetColor("_TintColor", new Color(1, 1, 1, .5f));
            m_lineMaterial.SetPass(0);

            if (m_selected == index)
            {
                GL.Begin(GL.QUADS);
                GL.Color(new Color(0.25f, .5f, 1.0f, .25f));
                GL.Vertex3(rect.xMin, rect.yMin, 0);
                GL.Vertex3(rect.xMax, rect.yMin, 0);
                GL.Vertex3(rect.xMax, rect.yMax, 0);
                GL.Vertex3(rect.xMin, rect.yMax, 0);
                GL.End();

            }

            if (drawIndent)
            {
                GL.Begin(GL.LINES);
                GL.Color(new Color(1.0f, 1.0f, 1.0f, .25f));
                GL.Vertex3(rect.xMin, parentRect.yMax, 0);
                GL.Vertex3(rect.xMin, rect.yMax, 0);
                GL.Color(new Color(0.12f, .24f, .5f, .25f));
                GL.Vertex3(rect.xMin+1, parentRect.yMax, 0);
                GL.Vertex3(rect.xMin+1, rect.yMax, 0);
                GL.End();
            }

            EditorGUILayout.EndHorizontal();

            index++;

            if (node.childCount > 0)
            {
                GL.Begin(GL.LINES);
                GL.Color(new Color(1.0f, 1.0f, 1.0f, .25f));
                GL.Vertex3(rect.xMin, rect.yMax, 0);
                GL.Vertex3(rect.xMin + 14, rect.yMax, 0);
                GL.Color(new Color(0.12f, .24f, .5f, .25f));
                GL.Vertex3(rect.xMin, rect.yMax+1, 0);
                GL.Vertex3(rect.xMin + 14, rect.yMax+1, 0);
                GL.End();
                
                //if (GetShowChildren(element))
                {
                    indent += 14;
                    for (int i = 0; i < node.childCount; ++i)
                        HierarchyGUI(ref index, ref m_system.Nodes[index], rect, i == node.childCount - 1);
                    indent -= 14;
                }
            }
        }

        private void ElementEditor(ref UINode element)
        {
            //string selectedHierarchy = "";
            //var e = element;
            //while (e != null)
            //{
            //    selectedHierarchy = e.name + "/" + selectedHierarchy;
            //    e = e.parent;
            //}
            //EditorGUILayout.LabelField(selectedHierarchy);

            //if (element.parent != null)
            //{
            //    EditorGUILayout.BeginHorizontal();
            //    if (GUILayout.Button("Move Up"))
            //    {
            //        int index = element.parent.children.IndexOf(element);
            //        if (index > 0)
            //        {
            //            element.parent.children.RemoveAt(index);
            //            element.parent.children.Insert(index - 1, element);
            //        }
            //    }
            //
            //    if (GUILayout.Button("Move Down"))
            //    {
            //        int index = element.parent.children.IndexOf(element);
            //        if (index < element.parent.children.Count-1)
            //        {
            //            element.parent.children.RemoveAt(index);
            //            element.parent.children.Insert(index + 1, element);
            //        }
            //    }
            //    EditorGUILayout.EndHorizontal();
            //}

            EditorGUIUtility.labelWidth = 50;
            StringField("Name", ref element.name);
            EditorGUILayout.BeginHorizontal();
            BoolField("Enabled", ref element.enabled);
            BoolField("Visible", ref element.visible);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Separator();

            EditorGUILayout.LabelField("Content", EditorStyles.boldLabel);

            BoolField("Break batch", ref element.breakBatch);

            element.contentType = (ContentType)EditorGUILayout.EnumPopup("Type", element.contentType);

            //Object asset = AssetUtil.LoadAssetAtPath(element.assetPath, element.assetName);
            string assetPath = AssetDatabase.GUIDToAssetPath(element.assetGUID);
            Object asset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(Object));
            Object newAsset = asset;

            switch (element.contentType)
            {
                //case ContentType.Sprite:
                //case ContentType.SlicedSprite:
                //    var guiSprite = element.GetContent<GUISprite>();
                //    newAsset = EditorGUILayout.ObjectField("Sprite", asset, typeof(Sprite), false, null);
                //    guiSprite.color = new GUIColor(EditorGUILayout.ColorField("Color", guiSprite.color.UnityColor));
                //    break;
                //case ContentType.Text:
                //    var guiText = element.GetContent<GUIText>();
                //
                //    newAsset = EditorGUILayout.ObjectField("Font", asset, typeof(Font), false, null);
                //    StringField("Text", ref guiText.text);
                //    IntField("Size", ref guiText.fontSize);
                //    guiText.textAnchor = (TextAnchor)EditorGUILayout.EnumPopup("Anchor", guiText.textAnchor);
                //    guiText.color = new GUIColor(EditorGUILayout.ColorField("Color", guiText.color.UnityColor));
                //    break;
                //case ContentType.GUIAsset:
                //    newAsset = EditorGUILayout.ObjectField("GUI", asset, typeof(GUIAsset), false, null);
                //
                //    if (newAsset == m_UIAsset)
                //        newAsset = null;
                //    break;
            }

            if (newAsset != asset)
            {
                asset = newAsset;
                if (asset != null)
                {
                    assetPath = AssetDatabase.GetAssetPath(asset);
                    element.assetGUID = AssetDatabase.AssetPathToGUID(assetPath);
                    //element.assetPath = AssetDatabase.GetAssetPath(asset);
                    //element.assetName = asset.name;

                    // Update asset references
                    m_UIAsset.AddReferences(m_system);
                }
                else
                {
                    element.assetGUID = null;
                    //element.assetPath = null;
                    //element.assetName = null;
                }
            }

            EditorGUILayout.Separator();

            EditorGUILayout.LabelField("Layout", EditorStyles.boldLabel);
       
            IntField("Width", ref element.width);
            IntField("Height", ref element.height);
            IntField("Depth", ref element.depth);

            EditorGUILayout.LabelField("Margin");
            EditorGUILayout.BeginHorizontal();
            EditorGUIUtility.labelWidth = 12;
            IntField("L", ref element.margin.left, GUILayout.Width(45));
            IntField("T", ref element.margin.top, GUILayout.Width(45));
            IntField("R", ref element.margin.right, GUILayout.Width(45));
            IntField("B", ref element.margin.bottom, GUILayout.Width(45));
            EditorGUILayout.EndHorizontal();

            EditorGUIUtility.labelWidth = 0;
            element.alignment.x = (HorizontalAlignment)EditorGUILayout.EnumPopup("Horizontal alignment", element.alignment.x);
            element.alignment.y = (VerticalAlignment)EditorGUILayout.EnumPopup("Vertical alignment", element.alignment.y);

            element.layoutType = (LayoutType)EditorGUILayout.EnumPopup("Layout", element.layoutType);

        }

        private void IntField(string label, ref int value, params GUILayoutOption[] options)
        {
            value = EditorGUILayout.IntField(label, value, options);
        }

        private void IntField(ref int value)
        {
            value = EditorGUILayout.IntField(value);
        }

        private void BoolField(string label, ref bool value)
        {
            value = EditorGUILayout.Toggle(label, value);
        }

        private void StringField(string label, ref string value)
        {
            value = EditorGUILayout.TextField(label, value);
        }

        private Material m_lineMaterial;

        private int indent;
        private int m_selected;

        private Vector2 m_hierarchyScroll;
        private Vector2 m_inspectorScroll;

        private UIAsset m_UIAsset;
        private UISystem m_system;

    }
}
