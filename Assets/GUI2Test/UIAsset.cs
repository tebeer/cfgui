﻿using UnityEngine;
using JsonFx.Json;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CFUI
{
    [System.Serializable]
    public struct AssetReference
    {
        public string guid;
        public Object asset;
    }

    [CreateAssetMenu]
    public class UIAsset : ScriptableObject
    {
        public string data;

        public Object TryGetAsset(string guid)
        {
            if (m_assetReferences == null)
                return null;

            for (int i = 0; i < m_assetReferences.Count; ++i)
                if (m_assetReferences[i].guid == guid)
                    return m_assetReferences[i].asset;
            return null;
        }

        public Object GetAsset(string guid)
        {
            for (int i = 0; i < m_assetReferences.Count; ++i)
                if (m_assetReferences[i].guid == guid)
                    return m_assetReferences[i].asset;
            throw new System.Exception("Asset not found: " + guid);
        }

#if UNITY_EDITOR
        public void Serialize(UISystem system)
        {

        }

        public void AddReferences(UISystem system, string elementPath = "")
        {

        }

#endif

        [SerializeField]
        private List<AssetReference> m_assetReferences;
    }
}