﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using CFGUI;

namespace CFUI
{

    public enum HierarchyToken : int
    {
        None,
        Push,
        Pop,
    }

    public struct UINode
    {
        public string name;

        //public HierarchyToken token;
        public int childCount;
        //public int siblingIndex;

        public bool enabled;
        public bool visible;

        public Margin margin;
        public Alignment alignment;
        public LayoutType layoutType;
        public int width, height;
        public int depth;

        public bool breakBatch;

        public ContentType contentType;

        public object content;

        public string assetGUID;

    }

    public class UISystem
    {
        public int NodeCount { get { return m_nodeCount; } }

        public UISystem()
        {
            Nodes = new UINode[1];
            InsertNode(0);
            Nodes[0].name = "Root";
            //Nodes[0].siblingIndex = -1;
        }

        public int AddChild(int parentIndex)
        {
            Nodes[parentIndex].childCount++;

            var childIndex = parentIndex + 1;
            InsertNode(childIndex);

            //Nodes[childIndex].siblingIndex = Nodes[parentIndex].childCount;

            return childIndex;
        }

        //public int AddTwin(int index)
        //{
        //    if (index == 0)
        //        throw new System.Exception("Cannot add second root");
        //
        //    return AddChild(index - Nodes[index].siblingIndex);
        //}

        public void MoveUp(int index)
        {

        }

        private void InsertNode(int index)
        {
            if (m_nodeCount >= Nodes.Length)
            {
                var newNodes = new UINode[Nodes.Length * 2];
                System.Array.Copy(Nodes, newNodes, Nodes.Length);
                Nodes = newNodes;
            }

            if (index != m_nodeCount)
            {
                System.Array.Copy(Nodes, index, Nodes, index + 1, m_nodeCount - index);
            }

            Nodes[index] = new UINode();

            m_nodeCount++;
        }

        public void RemoveNode(int index)
        {
            if (Nodes.Length > 1)
            {
                System.Array.Copy(Nodes, index + 1, Nodes, index, m_nodeCount - index - 1);
            }
            m_nodeCount--;
        }

        private int m_nodeCount;
        public UINode[] Nodes;
        private UINode[] Temp;
    }
}