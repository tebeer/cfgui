Root
 Name: Root
 Tag: 
 Enabled: True
 Visible: False
 Margin
  Left: 0
  Top: 0
  Right: 0
  Bottom: 0
 Alignment
  x: 3
  y: 3
 LayoutType: 0
 Width: 128
 Height: 128
 Depth: 0
 BreakBatch: False
 ContentType: 0
 Content
  Color
   r: 1
   g: 1
   b: 1
   a: 1
 AssetGUID: 
 ChildCount: 3
 0
  Name: TopBar
  Tag: 
  Enabled: True
  Visible: False
  Margin
   Left: 0
   Top: 0
   Right: 0
   Bottom: 0
  Alignment
   x: 3
   y: 0
  LayoutType: 2
  Width: 600
  Height: 60
  Depth: 0
  BreakBatch: False
  ContentType: 0
  Content
   Color
    r: 1
    g: 1
    b: 1
    a: 1
  AssetGUID: 
  ChildCount: 2
  0
   Name: AddButton
   Tag: AddButton
   Enabled: True
   Visible: True
   Margin
    Left: 0
    Top: 0
    Right: 0
    Bottom: 0
   Alignment
    x: 3
    y: 3
   LayoutType: 0
   Width: 32
   Height: 32
   Depth: 0
   BreakBatch: False
   ContentType: 0
   Content
    Color
     r: 0
     g: 0
     b: 0
     a: 0
   AssetGUID: 
   ChildCount: 2
   0
    Name: Base
    Tag: 
    Enabled: True
    Visible: True
    Margin
     Left: 0
     Top: 0
     Right: 0
     Bottom: 0
    Alignment
     x: 3
     y: 3
    LayoutType: 0
    Width: 32
    Height: 32
    Depth: 0
    BreakBatch: False
    ContentType: 3
    Content
    AssetGUID: b4caf91364d8fbc428443ab1177b1e8d
    ChildCount: 0
   1
    Name: Text
    Tag: 
    Enabled: True
    Visible: True
    Margin
     Left: 0
     Top: 0
     Right: 0
     Bottom: 0
    Alignment
     x: 3
     y: 3
    LayoutType: 0
    Width: 32
    Height: 32
    Depth: 0
    BreakBatch: False
    ContentType: 2
    Content
     Color
      r: 255
      g: 255
      b: 255
      a: 255
     Text: Add
     TextAnchor: 4
     FontSize: 32
    AssetGUID: 39377d1756247fa4d9576edf21a641fa
    ChildCount: 0
  1
   Name: GUIElement
   Tag: OtherButton
   Enabled: True
   Visible: True
   Margin
    Left: 0
    Top: 0
    Right: 0
    Bottom: 0
   Alignment
    x: 3
    y: 3
   LayoutType: 0
   Width: 32
   Height: 32
   Depth: 0
   BreakBatch: False
   ContentType: 3
   Content
   AssetGUID: 750908d701766d243833a11aacecb34d
   ChildCount: 0
 1
  Name: UserList
  Tag: UserList
  Enabled: True
  Visible: True
  Margin
   Left: 0
   Top: 60
   Right: 0
   Bottom: 0
  Alignment
   x: 3
   y: 0
  LayoutType: 3
  Width: 32
  Height: 280
  Depth: 0
  BreakBatch: False
  ContentType: 0
  Content
   Color
    r: 0
    g: 0
    b: 0
    a: 0
  AssetGUID: 
  ChildCount: 1
  0
   Name: TestName
   Tag: TestName
   Enabled: True
   Visible: True
   Margin
    Left: 0
    Top: 0
    Right: 0
    Bottom: 0
   Alignment
    x: 3
    y: 3
   LayoutType: 0
   Width: 32
   Height: 32
   Depth: 0
   BreakBatch: False
   ContentType: 3
   Content
   AssetGUID: 74b671b2ca9e24443a374d6ec56694f0
   ChildCount: 0
 2
  Name: GUIElement
  Tag: 
  Enabled: True
  Visible: True
  Margin
   Left: 0
   Top: 0
   Right: 0
   Bottom: 0
  Alignment
   x: 2
   y: 2
  LayoutType: 0
  Width: 194
  Height: 329
  Depth: 0
  BreakBatch: False
  ContentType: 1
  Content
   Color
    r: 255
    g: 255
    b: 255
    a: 255
  AssetGUID: 4b9847070f64a294db26632353923e61
  ChildCount: 1
  0
   Name: GUIElement
   Tag: 
   Enabled: True
   Visible: True
   Margin
    Left: 6
    Top: 6
    Right: 6
    Bottom: 6
   Alignment
    x: 3
    y: 3
   LayoutType: 3
   Width: 114
   Height: 296
   Depth: 0
   BreakBatch: False
   ContentType: 4
   Content
    Axis: 0
   AssetGUID: 
   ChildCount: 7
   0
    Name: GUIElement
    Tag: 
    Enabled: True
    Visible: True
    Margin
     Left: 0
     Top: 0
     Right: 0
     Bottom: 0
    Alignment
     x: 3
     y: 2
    LayoutType: 0
    Width: 23
    Height: 32
    Depth: 0
    BreakBatch: False
    ContentType: 3
    Content
    AssetGUID: 750908d701766d243833a11aacecb34d
    ChildCount: 0
   1
    Name: GUIElement
    Tag: 
    Enabled: True
    Visible: True
    Margin
     Left: 0
     Top: 0
     Right: 0
     Bottom: 0
    Alignment
     x: 3
     y: 3
    LayoutType: 0
    Width: 23
    Height: 32
    Depth: 0
    BreakBatch: False
    ContentType: 3
    Content
    AssetGUID: 750908d701766d243833a11aacecb34d
    ChildCount: 0
   2
    Name: GUIElement
    Tag: 
    Enabled: True
    Visible: True
    Margin
     Left: 0
     Top: 0
     Right: 0
     Bottom: 0
    Alignment
     x: 3
     y: 3
    LayoutType: 0
    Width: 23
    Height: 32
    Depth: 0
    BreakBatch: False
    ContentType: 3
    Content
    AssetGUID: 750908d701766d243833a11aacecb34d
    ChildCount: 0
   3
    Name: GUIElement
    Tag: 
    Enabled: True
    Visible: True
    Margin
     Left: 0
     Top: 0
     Right: 0
     Bottom: 0
    Alignment
     x: 3
     y: 3
    LayoutType: 0
    Width: 23
    Height: 32
    Depth: 0
    BreakBatch: False
    ContentType: 3
    Content
    AssetGUID: 750908d701766d243833a11aacecb34d
    ChildCount: 0
   4
    Name: GUIElement
    Tag: 
    Enabled: True
    Visible: True
    Margin
     Left: 0
     Top: 0
     Right: 0
     Bottom: 0
    Alignment
     x: 3
     y: 3
    LayoutType: 0
    Width: 23
    Height: 32
    Depth: 0
    BreakBatch: False
    ContentType: 3
    Content
    AssetGUID: 750908d701766d243833a11aacecb34d
    ChildCount: 0
   5
    Name: GUIElement
    Tag: 
    Enabled: True
    Visible: True
    Margin
     Left: 0
     Top: 0
     Right: 0
     Bottom: 0
    Alignment
     x: 3
     y: 3
    LayoutType: 0
    Width: 23
    Height: 32
    Depth: 0
    BreakBatch: False
    ContentType: 3
    Content
    AssetGUID: 750908d701766d243833a11aacecb34d
    ChildCount: 0
   6
    Name: GUIElement
    Tag: 
    Enabled: True
    Visible: True
    Margin
     Left: 0
     Top: 0
     Right: 0
     Bottom: 0
    Alignment
     x: 3
     y: 3
    LayoutType: 0
    Width: 23
    Height: 32
    Depth: 0
    BreakBatch: False
    ContentType: 3
    Content
    AssetGUID: 750908d701766d243833a11aacecb34d
    ChildCount: 0
