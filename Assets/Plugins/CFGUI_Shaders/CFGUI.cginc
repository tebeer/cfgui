﻿half4 CFGUI_CLIPRECT;


#if CLIP_ON
inline void ClipRect(half4 pos)
{
	if (pos.x < CFGUI_CLIPRECT.x ||
		pos.x > CFGUI_CLIPRECT.y ||
		pos.y < CFGUI_CLIPRECT.z ||
		pos.y > CFGUI_CLIPRECT.w)
		clip(-1.0);
}
#else
#define ClipRect(pos)
#endif