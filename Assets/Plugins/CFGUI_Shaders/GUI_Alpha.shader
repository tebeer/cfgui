﻿Shader "CFGUI/GUI_Alpha"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Blend One OneMinusSrcAlpha
		ZTest LEqual
		ZWrite Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile __ CLIP_ON
			#include "CFGUI.cginc"

			struct appdata
			{
				float4 pos : POSITION;
				float2 uv : TEXCOORD0;
				fixed4 color : COLOR;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				fixed4 color : COLOR;
#if CLIP_ON
				float4 clipPos : TEXCOORD1;
#endif
			};

			sampler2D _MainTex;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.pos);
				o.uv = v.uv;
				o.color = v.color;
#if CLIP_ON
				o.clipPos = o.pos;
#endif
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				ClipRect(i.clipPos);
			
				return i.color*tex2D(_MainTex, i.uv).a; // Alpha texture
			}
			ENDCG
		}
	}
}
