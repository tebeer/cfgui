using CFGUI;
namespace GUIImplementation
{
    public static partial class GUIBindings
    {
        public static void Init()
        {
            GUICodeBindings.Add<GUI_Button2_Binding>("Button2");
            GUICodeBindings.Add<GUI_ButtonBase_Binding>("ButtonBase");
            GUICodeBindings.Add<GUI_ButtonCancel_Binding>("ButtonCancel");
            GUICodeBindings.Add<GUI_ButtonOK_Binding>("ButtonOK");
            GUICodeBindings.Add<GUI_HUB>("HUB");
            GUICodeBindings.Add<GUI_NewGUIAsset_Binding>("NewGUIAsset");
            GUICodeBindings.Add<GUI_NewGUIAsset1_Binding>("NewGUIAsset1");
            GUICodeBindings.Add<GUI_TestGUI_Binding>("TestGUI");
            GUICodeBindings.Add<GUI_TopBar_Binding>("TopBar");
            GUICodeBindings.Add<GUI_UserName>("UserName");
        }
        public static GUI_Button2_Binding Create_Button2(GUIAsset asset, GUIEntity parent)
        {
            return GUICodeBindings.CreateGUI<GUI_Button2_Binding>(asset, parent);
        }
        public static GUI_ButtonBase_Binding Create_ButtonBase(GUIAsset asset, GUIEntity parent)
        {
            return GUICodeBindings.CreateGUI<GUI_ButtonBase_Binding>(asset, parent);
        }
        public static GUI_ButtonCancel_Binding Create_ButtonCancel(GUIAsset asset, GUIEntity parent)
        {
            return GUICodeBindings.CreateGUI<GUI_ButtonCancel_Binding>(asset, parent);
        }
        public static GUI_ButtonOK_Binding Create_ButtonOK(GUIAsset asset, GUIEntity parent)
        {
            return GUICodeBindings.CreateGUI<GUI_ButtonOK_Binding>(asset, parent);
        }
        public static GUI_HUB Create_HUB(GUIAsset asset, GUIEntity parent)
        {
            return GUICodeBindings.CreateGUI<GUI_HUB>(asset, parent);
        }
        public static GUI_NewGUIAsset_Binding Create_NewGUIAsset(GUIAsset asset, GUIEntity parent)
        {
            return GUICodeBindings.CreateGUI<GUI_NewGUIAsset_Binding>(asset, parent);
        }
        public static GUI_NewGUIAsset1_Binding Create_NewGUIAsset1(GUIAsset asset, GUIEntity parent)
        {
            return GUICodeBindings.CreateGUI<GUI_NewGUIAsset1_Binding>(asset, parent);
        }
        public static GUI_TestGUI_Binding Create_TestGUI(GUIAsset asset, GUIEntity parent)
        {
            return GUICodeBindings.CreateGUI<GUI_TestGUI_Binding>(asset, parent);
        }
        public static GUI_TopBar_Binding Create_TopBar(GUIAsset asset, GUIEntity parent)
        {
            return GUICodeBindings.CreateGUI<GUI_TopBar_Binding>(asset, parent);
        }
        public static GUI_UserName Create_UserName(GUIAsset asset, GUIEntity parent)
        {
            return GUICodeBindings.CreateGUI<GUI_UserName>(asset, parent);
        }
    }
    public class GUI_Button2_Binding : GUICodeBinding
    {
    }
    public class GUI_ButtonBase_Binding : GUICodeBinding
    {
    }
    public class GUI_ButtonCancel_Binding : GUICodeBinding
    {
    }
    public class GUI_ButtonOK_Binding : GUICodeBinding
    {
        private static readonly int[] ButtonTextPath = { 1 };
        public GUIEntity ButtonText;
        public GUIText ButtonTextText;
        protected override void InitRefs()
        {
            ButtonText = m_root.GetChild(ButtonTextPath);
            ButtonTextText = ButtonText.GetContent<GUIText>();
        }
    }
    public class GUI_HUB_Binding : GUICodeBinding
    {
        private static readonly int[] AddButtonPath = { 0, 0 };
        public GUIEntity AddButton;
        public GUISprite AddButtonSprite;
        private static readonly int[] OtherButtonPath = { 0, 1 };
        public GUI_ButtonOK_Binding OtherButton;
        private static readonly int[] UserListPath = { 1 };
        public GUIEntity UserList;
        public GUISprite UserListSprite;
        private static readonly int[] TestNamePath = { 1, 0 };
        public GUI_UserName TestName;
        protected override void InitRefs()
        {
            AddButton = m_root.GetChild(AddButtonPath);
            AddButtonSprite = AddButton.GetContent<GUISprite>();
            OtherButton = m_root.GetChild(OtherButtonPath).children[0].binding as GUI_ButtonOK_Binding;
            UserList = m_root.GetChild(UserListPath);
            UserListSprite = UserList.GetContent<GUISprite>();
            TestName = m_root.GetChild(TestNamePath).children[0].binding as GUI_UserName;
        }
    }
    public class GUI_NewGUIAsset_Binding : GUICodeBinding
    {
    }
    public class GUI_NewGUIAsset1_Binding : GUICodeBinding
    {
    }
    public class GUI_TestGUI_Binding : GUICodeBinding
    {
    }
    public class GUI_TopBar_Binding : GUICodeBinding
    {
    }
    public class GUI_UserName_Binding : GUICodeBinding
    {
        private static readonly int[] NamePath = { 1 };
        public GUIEntity Name;
        public GUIText NameText;
        protected override void InitRefs()
        {
            Name = m_root.GetChild(NamePath);
            NameText = Name.GetContent<GUIText>();
        }
    }
}