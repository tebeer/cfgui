﻿using CFGUI;

namespace GUIImplementation
{
    public class GUI_UserName : GUI_UserName_Binding
    {
        public override void OnCreated(GUIEntity root)
        {
            UnityEngine.Debug.Log("GUI_UserName.OnCreated");
            NameText.text = "[DEV]tebeer";

            root.parent.EvtClicked += () =>
            {
                UnityEngine.Debug.Log("Click " + root.parent.name);
                root.parent.Destroy();
            };
        }
    }
}