﻿using UnityEngine;
using CFGUI;

namespace GUIImplementation
{
    public class GUI_HUB : GUI_HUB_Binding
    {
        public override void OnCreated(GUIEntity root)
        {
            Debug.Log("HUB_Implementation.OnCreated");
            //Debug.Log(m_root.GetChild(TestNamePath).name);
            TestName.NameText.text = "Moro";

            OtherButton.ButtonTextText.text = "Button";

            GUIAsset userNameAsset = Resources.Load<GUIAsset>("GUIAssets/UserName");

            foreach(var name in names)
            {
                //var e = UserList.AddNestedChild(userNameAsset);
                var userName = GUICodeBindings.CreateGUI<GUI_UserName>(userNameAsset, UserList);
                userName.NameText.text = name;
            }

            AddButton.EvtClicked += () =>
            {
                var userName = GUIBindings.Create_UserName(userNameAsset, UserList);

                string str = "asd" + Random.value.ToString();
                userName.NameText.text = str;
            };

        }

        private static string[] names = new string[] { "avcc", "def", "fghh" };
    }
}